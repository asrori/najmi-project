{barangs}
<div align="center">
<h2>Edit Barang</h2>
<form action="" method="">
<table>
	<tr>
		<td>ID Barang</td>
		<td><input type="text" value="{barang_id}" /></td>
	</tr>
	<tr>
		<td>Nama</td>
		<td><input type="text" value="{barang_nama}" /></td>
	<tr>
	<tr>
		<td>Harga</td>
		<td><input type="text" value="{barang_harga}" /></td>
	</tr>
	<tr>
		<td>Deskripsi</td>
		<td><textarea cols="50px" rows="10px">{barang_deskripsi}</textarea></td>
	</tr>
	<tr>
		<td>Satuan : {barang_satuan}</td>
		<td><!--input value="{barang_satuan}" /-->
			<select>
				<option value="Unit" <?php echo "{barang_satuan}" == "Unit" ? "selected" : "" ?>>Unit</option>
				<option value="Pack" <?php echo "{barang_satuan}" == "Pack" ? "selected" : "" ?>>Pack</option>
				<option value="Gram" <?php echo "{barang_satuan}" == "Gram" ? "selected" : "" ?>>Gram</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Gambar : {barang_gambar}</td>
		<td><input type="file" value="{barang_gambar}" /></td>
	</tr>
	<tr>
		<td><input class="button" type="button" value="Batal" /></td>
		<td><input class="button" type="submit" value="Simpan" /></td>
	</tr>
</table>
</form>
</div>
{/barangs}