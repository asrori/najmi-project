<div align="center">
{barangs}
<div class="detail_barang">
    <h2>{barang_nama}</h2>
    <strong>Kode: {barang_id}</strong>
    <img src="<?php echo base_url('images/produk/{barang_gambar}');?>" />
    <p class="title">Harga Rp. {barang_harga}</p>
    <span class="deskripsi">{barang_deskripsi}</span>
    <p><?php
        echo anchor('cart/add/{barang_id}', 'Beli', array('class'=> 'button', 'title' => 'Klik untuk memasukkan ke &quot;Keranjang Belanja&quot; dan membeli'));
        echo anchor('barang/show', 'Lainnya', array('class'=> 'button', 'title' => 'Klik untuk melihat Produk Lainnya'));
    ?></p>
</div>
{/barangs}
</div>