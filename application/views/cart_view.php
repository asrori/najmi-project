<strong>{pesan}</strong>
<p class="title">Keranjang Belanja</p>
<table align="center">
    <tr>
	    <th>Kode</th>
	    <th>Nama</th>
	    <th>Harga</th>
	    <th>Qty</th>
		<th>Jumlah</th>
	</tr>
	<?php echo form_open('cart/edit');?>
	{barangs}
	<?php echo form_hidden('rowid_{rowid}', '{rowid}');?>
	<tr>
	    <td>{id}</td>
	    <td>{name}</td>
	    <td align="right">{price}</td>
	    <td><?php echo form_input(array('type'=> 'number','name'=> 'qty_{rowid}','value'=>'{qty}', 'size'=> '5'));?></td>
	    <td align="right">{subtotal}</td>
	</tr>
	{/barangs}
	<tr>
	     <td colspan="4"><b>Total</b></td>
		 <td class="total"><strong>Rp. {total}</strong></td>
	</tr>
	<tr>
	    <td colspan="5"><?php echo form_submit(array('name'=> 'ubah', 'value'=> 'Ubah', 'class'=>'button', 'title' => 'Klik untuk mengubah Qty'));?></td>
	</tr>
	</table>
	
	<p><strong>Belanja? </strong>
	<?php
	     echo anchor('barang/show', 'Lagi', array('class' => 'button', 'title' => 'Klik untuk berbelanja lagi'));
	     echo anchor('cart/finished', 'Selesai', array('class' => 'button', 'title' => 'Klik untuk membeli'));
	?>
	</p>
<?php echo form_close();