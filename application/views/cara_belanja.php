<p class="title">Cara Belanja</p>
<ol align="left">
    <li>Silakan pilih produk dengan mengeklik tombol <b>Beli</b></li>
    <li>Tunggu konfirmasi pesanan (invoice) dari kami (via email)</li>
    <li>Transfer dana yang tertera ke rekening BCA, MANDIRI, BRI, DANAMON </li>
    <li>Konfirmasi bukti pembayaran via E-Mail (BELICAMERA.com)</li>
</ol>
<p class="title">Kami akan segera proses pengiriman barang anda</p>