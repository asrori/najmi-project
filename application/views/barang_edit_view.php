{barangs}
<?php
echo isset($error) ? "<font class='error'>File Upload Error! ... status kesalahan : $error</font>" : ""; // operator ternary untuk mengecek eror
//echo isset($upload_data) ? br(2)."<font class='succes'>File Berhasil di Upload : </font>".print_r($upload_data) : "";
if(isset($upload_data)){
	echo br(2)."<font class='succes'>File Berhasil di Update : </font>";
	//print_r($upload_data);
}else{
	echo "";
}
echo br(1);
echo heading("Edit Barang",1,"class='header'");
//echo $pesan;
//ubah menjadi = form_open_multipart("barang/update");
echo form_open_multipart("barang/update");
	$attr = array(
		"class"	=>	"lb",
	);
	
	echo form_label("ID Barang : ","barang_id",$attr);
	$data = array(
				"name"	=>	"barang_id",
				"id"	=>	"barang_id",
				"value"	=>	"{barang_id}",
				"required" => "required",
	);
	echo form_input($data);
	echo br(2); // membuat alinea baru
	
	echo form_label("Nama Barang : ","barang_nama",$attr);
	$data = array(
				"name"	=>	"barang_nama",
				"id"	=>	"barang_nama",
				"size"	=>	"50",
				"value"	=>	"{barang_nama}",
				"required" => "required",
	);
	echo form_input($data);
	echo br(2);
	
	echo form_label("Harga : ","barang_harga",$attr);
	$data = array(
				"name"	=>	"barang_harga",
				"id"	=>	"barang_harga",
				"size"	=>	"10",
				"type"	=>	"number",
				"value"	=> "{barang_harga}",
				"required" => "required",
	);
	echo form_input($data);
	echo br(2);
	
	echo form_label("Deskripsi : ","barang_deskripsi",$attr);
	$data = array(
				"name"	=>	"barang_deskripsi",
				"id"	=>	"barang_deskripsi",
				"value"	=>	"{barang_deskripsi}",
				"rows"	=>	"10",
				"cols"	=>	"50",
				"required" => "required",
	);
	echo form_textarea($data);
	echo br(2);
	
	echo form_label("Satuan : ","barang_satuan",$attr);
	$data = array(
				"name"	=>	"barang_satuan",
				"id"	=>	"barang_satuan",
				"size"	=>	"15",
				"value"	=>	"{barang_satuan}",
				"required" => "required",
	);
	$satuan = array(
				'Unit'	=>	'Unit',
				'Pack'	=>	'Pack',
				'Gram'	=>	'Gram',
	);
	echo form_dropdown("barang_satuan",$satuan,"Pack","id='barang_satuan'");
	echo br(2);
	
	echo form_label("Gambar : ","barang_foto",$attr);
	$data = array(
				"name"	=>	"barang_foto",
				"id"	=>	"barang_foto",
				"value"	=>	"{barang_foto}",
				//"required" => "required",
	);
	
	echo form_upload($data);
	echo br(2);
	
	echo form_submit("btnSubmit","Simpan");
	echo nbs(5); //and big space
	echo form_reset("btnReset","Reset");
echo form_close();

//echo anchor('customer_controller/customer','Tambah Customer','class="link-class"');
?>
{/barangs}