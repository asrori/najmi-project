<html>
    <head>
        <title>Tempat Penjualan Kamera Terbaik</title>
        <?php echo link_tag('assets/css/style_web.css');?>
    </head>
    <body>
        <div class="container" id="header">
            <h1 align="center">&nbsp<br>&nbsp<br>&nbsp<br>&nbsp<br>&nbsp<br>&nbsp<br>&nbsp<br></h1>
        </div>
        <div class="container" id="menu">
            <ul>
                <li>
					<?php
					$image_properties = array(
						'src'	=> 'assets/images/home_48.png',
						'alt'	=> 'beranda',
						'class'	=> 'beranda',
						'width'	=> '20',
						'height'=> '20',
						'title'	=> 'Beranda',
						'rel'	=> '',
					);
					
					?>
				</li>
				<li><?php
					echo anchor(base_url(), 'Beranda', array('title' => 'Klik untuk melihat halaman Beranda'));
				?></li>
                <li><?php
					echo anchor(site_url('cart/how_shop'), 'Cara Belanja', array('title' => 'Klik untuk melihat halaman Cara Belanja'));
				?></li>
                <li><?php
					echo anchor(site_url('cart'), 'Keranjang Belanja', array('title' => 'Klik untuk melihat halaman Keranjang Belanja'));
				?></li>
                <!--li><!?php echo anchor(site_url('customer'), 'Registrasi');?></li-->
				<li>
					<?php
					if(!is_login()){ //jika belum login
						echo anchor(site_url('customer/form_login'), 'Login Customer', array('title' => 'Login untuk Customer'));
					}else{ //jika sudah login
						echo anchor(site_url('customer/logout'), 'Logout Customer', array('title' => 'Logout untuk Customer'));
					}
					?>
				</li>
				<li>
					<?php
					if(!is_admin()){ //jika bukan admin
						echo anchor(site_url('admin/form_login'), 'Login Admin', array('title' => 'Login untuk Admin'));
					}else{ //jika admin
						echo anchor(site_url('barang/tambah'), 'Tambah Barang');
						echo anchor(site_url('admin/logout'), 'Logout Admin', array('title' => 'Logout untuk Admin'));
					}
					?>
				</li>
            </ul>
			
			
			
        </div>
        <div class="container">
		
            {content}
        </div>
		<div id="footer">
			Copyright Mohammad Najmi - 12.240.0076 - 7P45
		</div>
		
        
    </body>
</html>
