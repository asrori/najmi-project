<p class="title">Admin</p>
<div><?php echo validation_errors(); ?></div>
<table class="admin" align="center">
	<?php echo form_open('admin/login'); ?>
	<tr>
		<td><?php echo form_label('Username', 'username'); ?></td>
		<td><?php echo form_input(array('name'=> 'username', 'id'=> 'username', 'size'=> '25', 'required'=> 'required')); ?></td>
	</tr>
	<tr>
		<td><?php echo form_label('Password', 'password'); ?></td>
		<td><?php echo form_password(array('name'=> 'password', 'id'=> 'password', 'size'=> '25', 'required'=> 'required')); ?></td>
	</tr>
	<tr>
		<td colspan="5">
			<?php echo form_submit(array('name'=> 'login', 'value'=> 'Login', 'class'=> 'button')); ?>
			<?php echo form_reset(array('name'=> 'batal', 'value'=> 'Batal', 'class'=> 'button')); ?>
		</td>
	</tr>
</table>
<?php echo form_close(); ?>