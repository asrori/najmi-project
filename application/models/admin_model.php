<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Admin_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }    
    public function select(){
        return $this->db->query("SELECT * FROM admin ORDER BY admin_id");        
    }
	public function select_where($admin_nama= ''){
        return $this->db->query("SELECT * FROM admin WHERE admin_nama='".$admin_nama."'");        
    }
	public function insert($array = array(), $data){ //proses mengambil data inputan dari file admin_form_tambah
		$admin_nama	= $array['admin_nama'];
		$admin_password = $array['admin_password'];
		$this->db->set('admin_nama', $admin_nama);
		$this->db->set('admin_password', $admin_password);
		$this->db->insert('admin'); //proses input ke database, tabel admin
	}
}