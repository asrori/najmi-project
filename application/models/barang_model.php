<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Barang_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }    
    public function select(){
        return $this->db->query("SELECT * FROM barang WHERE barang_aktif='1' ORDER BY barang_id");        
    }
    public function select_where($barang_id= ''){
        return $this->db->query("SELECT * FROM barang WHERE barang_id='".$barang_id."'");        
    }
	public function buy_select($barang_id= ''){
		$this->db->select("barang_id AS id, barang_harga AS price, barang_nama AS name");
		$this->db->from('barang');
		$this->db->where(array('barang_id'=> $barang_id, 'barang_aktif'=>'1'));
		return $this->db->get();
	}
	public function insert($array = array(), $data){ //proses mengambil data inputan dari file barang_form_tambah
		$barang_id		= $array['barang_id'];
		$barang_nama	= $array['barang_nama'];
		$barang_harga	= $array['barang_harga'];
		$barang_satuan	= $array['barang_satuan'];
		$barang_deskripsi= $array['barang_deskripsi'];
		$barang_gambar	= $data['upload_data']['file_name'];
		$barang_aktif	= '1';
		$this->db->set('barang_id', $barang_id);
		$this->db->set('barang_nama', $barang_nama);
		$this->db->set('barang_harga', $barang_harga);
		$this->db->set('barang_satuan', $barang_satuan);
		$this->db->set('barang_deskripsi', $barang_deskripsi);
		$this->db->set('barang_gambar', $barang_gambar);
		$this->db->set('barang_aktif', $barang_aktif);
		$this->db->insert('barang'); //proses input ke database, tabel barang
	}
	
	//edit
	public function update($barang_id= null, $data = array())
	{
		if(!empty($data))
		{
			$this->db->where('barang_id', $barang_id);
			$this->db->update('barang', $data);
		}
	}
	//end edit
	
	//hapus
	public function hapus($id = null)
	{
		if(!empty($id))
		{
			$this->db->from('barang');
			$this->db->where('barang_id', $id);
			$this->db->delete();
		}
	}
	//end hapus
	
	public function select_cari($input)
		{
			$data = $input['barang_nama'];
			return $this->db->query("SELECT * FROM barang WHERE barang_nama LIKE '%".$data."%' AND barang_aktif = '1'");
		}
}