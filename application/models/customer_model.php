<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function insert($customer= array()){
		if(count($customer)>0){
			$this->db->insert('customer', $customer);
		}
	}
	public function select(){
		$this->db->select();
		$this->db-->form('customer');
		return $this->db->get();
	}
	public function select_where($customer_email=null){
		$this->db->select();
		$this->db->from('customer');
		$this->db->where('customer_email', $customer_email);
		return $this->db->get();
	}
}