<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
class Barang extends CI_Controller{
    function __construct() { 
        parent::__construct();
        $this->load->model('barang_model');
    }
    public function index(){
        $this->show();
		//$this->load->library('pagination');
        //$config['base_url'] = base_url().'index.php/barang/index/';
        //$config['total_rows'] = 20;
        //$config['per_page'] = 3; 
        //$this->pagination->initialize($config); 
        //$data['paging']     =$this->pagination->create_links();
        //$halaman            =  $this->uri->segment(3);
        //$halaman            =$halaman==''?0:$halaman;
        //$data['record']     =    $this->model_barang->tampilkan_data_paging($halaman,$config['per_page']);
        //$this->template->load('template','barang_view',$data);
		
    }
    public function show(){
		$data = array('barangs' => $this->barang_model->select()->result_array());
		foreach($data['barangs'] AS $urutan => $values){
			$data['barangs'][$urutan]['barang_harga'] = number_format($values['barang_harga'], 0, ',', '.');
		}
		$content= array(
			'content' => $this->parser->parse('barang_view', $data, true),
        );
		$this->parser->parse('template', $content);
    }
	public function show_detail($barang_id= ''){
        $data = array('barangs' => $this->barang_model->select_where($barang_id)->result_array());
        $content= array(
            'content' => $this->parser->parse('barang_detail_view', $data, true)
        );
        $this->parser->parse('template', $content);
    }
	public function tambah(){
		$data['pesan'] = '';
		$this->load->model('barang_model');
		if($this->input->post('btnSubmit')){
			$config['upload_path'] = './images/produk/'; // tempat menyimpan gambar
			$config['allowed_types'] = 'gif|jpg|png'; // tipe gambar yang bisa di upload
			$this->load->library('upload', $config);
			
			if( ! $this->upload->do_upload('barang_foto')) //kalau upload gagal
			{
				$error = array('error' => $this->upload->display_errors());
				
				$content = array(
						'content' => $this->parser->parse('barang_form_tambah', $error, true)
				);
				
				$this->parser->parse('template', $content);
			}
			else //kalau data berhasil diupload
			{
				$data = array('upload_data' => $this->upload->data()); // mencetak array data gambar yang di upload
				
				$data['pesan'] = $this->barang_model->insert($this->input->post(), $data); //memasukkan data ke barang_model, dengan nama fungsi insert()
				
				$content = array(
						'content' => $this->parser->parse('barang_form_tambah', $data, true)
				);
				
				$this->parser->parse('template', $content);
			}
		}else{
			$data['pesan'] = "";
			
			$content = array(
					'content' => $this->parser->parse('barang_form_tambah', $data, true)
				);
				
			$this->parser->parse('template', $content);
		}
	}
	
	// start edit
	public function edit($barang_id= ''){
		//$this->load->view("barang_detail_view");
		$data = array('barangs' => $this->barang_model->select_where($barang_id)->result_array());
        $content= array(
            'content' => $this->parser->parse('barang_edit_view', $data, true)
        );
        $this->parser->parse('template', $content);
	}
	
	public function update()
	{
		$barang_id = $this->input->post('barang_id');
		$data = array(
			'barang_nama' => $this->input->post('barang_nama'),
			'barang_harga' => $this->input->post('barang_harga'),
			'barang_deskripsi' => $this->input->post('barang_deskripsi'),
			'barang_satuan' => $this->input->post('barang_satuan'),
		);
		
		$this->barang_model->update($barang_id, $data);
		
		$config['upload_path'] = './images/produk/'; // tempat menyimpan gambar
		$config['allowed_types'] = 'gif|jpg|png'; // tipe gambar yang bisa di upload
		$config['file_name']	= url_title($this->input->post('barang_gambar'));
		$this->load->library('upload', $config);
			
		if( ! $this->upload->do_upload('barang_foto')) //kalau upload gagal
		{
			$error = array('error' => $this->upload->display_errors());
			$content = array(
					'content' => $this->parser->parse('barang_view', $error, true)
			);
			$this->parser->parse('template', $content);
			redirect('barang');
		}
		else //kalau data berhasil diupload
		{
			$gambar = array('barang_gambar' => $this->upload->file_name);
			$this->barang_model->update($barang_id, $gambar);
			redirect('barang');
		}
	}
	//end edit
	
	//start hapus
	public function hapus($id = null)
	{
		if(!empty($id))
		{
			$this->barang_model->hapus($id);
			redirect('barang');
		}
		else
		{
			redirect('barang');
		}
	}
	//end hapus
	
	public function show_cari()
		{
			if($this->input->post('cari'))
			{
				$data = array('barangs' => $this->barang->select_cari($this->input->post())->result_array());
				if ($this->barang->select_cari($this->input->post())->num_rows() == 0)
				{
					$input['pesan'] = $_POST['barang_nama'];
					$content = array('content' => $this->load->view('pesan_error', $input, true));
				}
				else
				{
					$content = array('content' => $this->parser->parse('barang_view', $data, true));
				}
				$this->parser->parse('template', $content);
			}
	
	}

	
}
