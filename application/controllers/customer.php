<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('customer_model');
	}
	public function index(){
		$this->show();
	}
	public function show(){
		$this->load->library('form_validation');
		$content = array(
				'content'=>$this->load->view('customer_registrasi_view', '', true)
		);
		$this->parser->parse('template', $content);
	}
	public function registrasi(){
		$this->load->library('form_validation');
		//setting rule
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		//setting message
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		$this->form_validation->set_message('valid_email', '%s bukan format email');
		if($this->form_validation->run()){
			$post= array(
				'customer_nama'		=> $this->input->post('nama'),
				'customer_alamat'		=> $this->input->post('alamat'),
				'customer_telepon'		=> $this->input->post('telepon'),
				'customer_email'		=> $this->input->post('email'),
				'customer_password'		=> $this->encrypt->encode($this->input->post('password')),
			);
			$this->customer_model->insert($post);
			$content= array(
				'content'=>"<h2>Registrasi berhasil</h2>".$this->load->view('customer_login_view', '', true)
			);
			
		}
		else
		{
			$content= array(
					'content'=> $this->load->view('customer_registrasi_view', '', true)
			);
		}
		$this->parser->parse('template', $content);
	}
	public function form_login(){
		$content = array(
					'content'=> $this->load->view('customer_login_view', '', true).anchor(site_url('customer/registrasi'), '<p>Registrasi Customer</p>')
		);
		$this->parser->parse('template', $content);
	}
	function login(){
		$post = array(
					'customer_email'=> $this->input->post('email'),
					'customer_password'=> $this->input->post('password')
		);
		$result= $this->customer_model->select_where($post['customer_email'])->row_array();
		if($result && $this->encrypt->decode($result['customer_password']) == $post['customer_password']){
			$session = array(
						'customer_id' => $result['customer_id'],
						'customer_email' => $result['customer_email']
			);
			$this->session->set_userdata($session);
			redirect();
		}else{
			$content = array(
						'content' => '<h2>Login Gagal</h2>'.$this->load->view('customer_login_view', '', true).anchor(site_url('customer/registrasi'), '<p>Registrasi Customer</p>')
			);
			$this->parser->parse('template', $content);
		}
	}
	function logout(){
		$this->session->unset_userdata('customer_id');
		$this->session->unset_userdata('customer_email');
		redirect();
	}
}