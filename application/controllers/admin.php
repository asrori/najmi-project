<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('admin_model');
	}
	public function index(){
		$this->show();
	}
	public function show(){
		$this->load->library('form_validation');
		$content = array(
				'content'=>$this->load->view('admin_registrasi_view', '', true)
		);
		$this->parser->parse('template', $content);
	}
	public function registrasi(){
		$this->load->library('form_validation');
		//setting rule
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		//setting message
		$this->form_validation->set_message('required', '%s tidak boleh kosong');
		//$this->form_validation->set_message('valid_nama', '%s bukan format email');
		if($this->form_validation->run()){
			$post= array(
				'admin_nama'		=> $this->input->post('nama'),
				'admin_password'	=> $this->encrypt->encode($this->input->post('password')),
			);
			$this->admin_model->insert($post);
			$content= array(
				'content'=>"<h2>Hai Admin, selamat datang</h2>".$this->load->view('admin_login_view', '', true)
			);
			
		}
		else
		{
			$content= array(
					'content'=> $this->load->view('admin_registrasi_view', '', true)
			);
		}
		$this->parser->parse('template', $content);
	}
	public function form_login(){
		$content = array(
					'content'=> $this->load->view('admin_login_view', '', true)
		);
		$this->parser->parse('template', $content);
	}
	function login(){
		$post = array(
					'admin_nama'=> $this->input->post('username'),
					'admin_password'=> $this->input->post('password')
		);
		$result= $this->admin_model->select_where($post['admin_nama'])->row_array();
		if($result && $this->encrypt->decode($result['admin_password']) == $post['admin_password']){
			$session = array(
						'admin_id' => $result['admin_id'],
						'admin_nama' => $result['admin_nama']
			);
			$this->session->set_userdata($session);
			redirect();
		}else{
			$content = array(
						'content' => '<h2>Login Gagal</h2>'.$this->load->view('admin_login_view', '', true)
			);
			$this->parser->parse('template', $content);
		}
	}
	function logout(){
		$this->session->unset_userdata('admin_id');
		$this->session->unset_userdata('admin_nama');
		redirect();
	}
}
