-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2016 at 04:44 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `web2_15`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`admin_id` int(11) NOT NULL,
  `admin_nama` varchar(40) NOT NULL,
  `admin_alamat` text NOT NULL,
  `admin_telepon` varchar(15) NOT NULL,
  `admin_email` varchar(40) NOT NULL,
  `admin_password` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_nama`, `admin_alamat`, `admin_telepon`, `admin_email`, `admin_password`) VALUES
(1, 'admin', '', '', '', 'A7JQ6zco+6ija3Mc84w5O/pyGyT+0amfcKyAtina2YG/3YM6gzccu9tf8cRNEiKTC/7/v7vGjU8nk4jnJRxWhg==');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE IF NOT EXISTS `barang` (
  `barang_id` varchar(10) NOT NULL,
  `barang_nama` varchar(40) NOT NULL,
  `barang_harga` int(11) NOT NULL,
  `barang_satuan` varchar(15) NOT NULL,
  `barang_deskripsi` text NOT NULL,
  `barang_gambar` varchar(100) NOT NULL,
  `barang_aktif` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`barang_id`, `barang_nama`, `barang_harga`, `barang_satuan`, `barang_deskripsi`, `barang_gambar`, `barang_aktif`) VALUES
('JM005', 'Alexandre Christie Jam Tangan Pria - Sil', 850000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Chronograph analog watch</li>\r\n   <li>Material : Stainless steel case & Link bracelet</li>\r\n   <li>Japan Quartz movement</li>\r\n   <li>Tampilan 3 chrono eyes pada center dial</li>\r\n   <li>Water resistant hingga 50 m</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Silver - 6344MCBSSBASL</b></p>\r\n\r\n\r\n<p>Alexandre Christie 6344MCBSSBASL Jam Tangan Pria, chronograph analog watch yang memadukan silver stainless steel case & link bracelet dan black-white dial berdetail time marking. Jam tangan yang didesain sporty dengan tampilan 3 chrono eyes pada center dial, white accent Arabic & index numeral, silver bezel, dan date display window pada posisi antara angka 4-5 ini menggunakan Japan Quartz movement serta memiliki water resistant 50 m. Garansi 12 bulan.</p>', 'Jam51.jpg', 1),
('JM004', 'Alexandre Christie Jam Tangan Pria AC636', 785000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Jam Tangan Pria</li>\r\n   <li>Jenis Gelang Stainless Steel</li>\r\n   <li>Anti Air 50m</li>\r\n   <li>Penunjuk Tanggal</li>\r\n   <li>Diameter 4,6cm</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria AC6366MSSB</b></p>\r\n\r\n\r\n<p>Alexandre Christie AC6366MSSB merupakan jam tangan dengan model yang casual, sehingga anda akan terlihat semakin stylish ketika jam ini dipakai di tangan Anda. Jam ini mempunyai gelang dengan material stainless steel.\r\nJam ini mempunyai fitur:  format waktu 12/24 jam, dan anti air 50m.</p>', 'Jam41.jpg', 1),
('JM003', 'Alexandre Christie Jam Tangan Pria Silve', 768000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>All solid stainless steel</li>\r\n   <li>Japan Quartz Movement Cal.92</li>\r\n   <li>Diameter 4,5cm</li>\r\n   <li>Tahan air 50M</li>\r\n   <li>Penunjuk tanggal di arah jam 4</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie - Jam Tangan Pria - Silver Putih - Stainless Steel - AC6343</b></p>\r\n\r\n\r\n<p>Jam Tangan Alexandre Christie 6343MCBTBSL, Jam Tangan Pria yang terbuat dari bahan Silver stainless steel (anti karat). Jam tangan ini menggunakan mesin Japan Quartz Movement Cal.92. Memiliki Chronograph yang berfungsi sebagai stopwatch dan date display window di arah jam 4Dengan diameter 4,5cm sangat cocok di pakai sehari hari maupun untuk menghadiri suatu acara. Jam tangan ini memiliki kemampuan tahan air hingga kedalaman 50MBergaransi Internasional selama 12 Bulan, kini bisa di dapatkan Hanya di Lazada.co.id untuk penawaran terbaik</p>', 'Jam31.jpg', 1),
('JM002', 'Alexandre Christie Jam Tangan Pria - Hit', 856700, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Analog 3 jarum</li>\r\n   <li>Fitur : Tanggal</li>\r\n   <li>Bahan : Karet</li>\r\n   <li>Ketahanan air hingga 50 meter</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Hitam-List Merah - Strap Karet - AC3237</b></p>\r\n\r\n\r\n<p>Jam tangan keren dengan tampilan gagah dan exclusive, bahan tali dari karet mineral yang kuat dan lentur sehingga nyaman pada saat digunakan meskipun dalam keadaan berkeringat. Mesin yang kuat dengan ketahanan air hingga 50 meter, dan ketahanan baterai hingga 2 tahun. Fitur tanggal yang menyempurnakan jam tangan ini membuat anda semakin percaya diri dan yakin bisa menemani di setiap aktifitas anda.</p>', 'Jam21.jpg', 1),
('JM001', 'Alexandre Christie Jam Tangan Pria - Sil', 1000000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Analog watch</li>\r\n   <li>Material stainless steel case & link bracelet</li>\r\n   <li>Japan Quartz movement</li>\r\n   <li>Tampilan 3 chrono eyes</li>\r\n   <li>Water resistant hingga 50 m</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Silver - 6345MCBTESL</b></p>\r\n\r\n\r\n<p>Alexandre Christie 6345MCBTESL Jam Tangan Pria, analog watch yang memadukan silver stainless steel case & link bracelet dan white dial. Jam tangan dengan tampilan 3 chrono eyes pada center dial, black-white Arabic & index numeral, black bezel berdetail Tachymeter dan date display window pada posisi angka 4-5 ini menggunakan Japan Quartz movement serta memiliki water resistant 50 m. Garansi 12 bulan.</p>', 'Jam11.jpg', 1),
('JM006', 'Alexandre Christie Jam Tangan Pria Hitam', 1800000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Diameter 4,5 cm</li>\r\n   <li>Tahan Air 50m</li>\r\n   <li>All stainless steel</li>\r\n   <li>Chronograph</li>\r\n   <li>Quartz Movement</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie - Jam Tangan Pria - FullBlack - Stainless Steel - AC6384MFB</b></p>\r\n\r\n\r\n<p>Jam tangan AC 6384 MFB adalah jam tangan persembahan dari Alexandre Christie yang diperuntukkan bagi Anda yang ingin tampil lebih gaya. Dengan material stainless steel pada case & strap nya, jam ini tentunya akan membuat siapapun yang memakainya terlihat lebih elegan dan menawan.   Tahan Air Produk jam tangan dari Alexandre Christie ini bersifat tahan air sehingga Anda tidak perlu khawatir akan kerusakan jam Anda ketika kehujanan.  Dengan Kaca Mineral Berkualitas Jam tangan persembahan dari Alexandre Christie ini di desain dengan sebaik mungkin dan dengan material yang sangat baik. Bagian utama jam terlindungi oleh kaca mineral yang berkualitas, sehingga jam lebih tahan terhadap benturan halus.</p>', 'Jam61.jpg', 1),
('JM007', 'Alexandre Christie Jam Tangan Pria - Sil', 1200000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Water resist 50M</li>\r\n   <li>Chronograph stopwatch</li>\r\n   <li>petunjuk jam/tanggal</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Silver - Strap Stainless Steel - AC-6141</b></p>\r\n\r\n\r\n<p>Jam tangan Alexandre Christie untuk Pria, dilengkapi dengan petunjuk jam / tanggal / Chronograph Stopwatch, Water Resist hingga kedalaman 50 meter, Diameter jam : 4.5cm, Tebal Jam : 1cm, Lebar Tali Rantai : 2.1cm. Bergaransi resmi 1 tahun.</p>', 'Jam71.jpg', 1),
('JM008', 'Alexandre Christie Jam Tangan Pria Silve', 2200000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Petunjuk Jam / Tanggal.</li>\r\n   <li>Chronograph Stopwatch.</li>\r\n   <li>Battery Quartz</li>\r\n   <li>Water Resist 50 Meter.</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie- Jam Tangan Pria- Silver Black- Stainless Steel- AC 6345 MSB</b></p>\r\n\r\n\r\n<p>Jam tangan Alexandre Christie untuk Pria, dilengkapi dengan petunjuk jam / tanggal / Chronograph Stopwatch, Water Resist hingga kedalaman 50 meter, Diameter jam : 4.4cm, Tebal Jam : 1cm, Lebar Tali Rantai : 2.2cm. Bergaransi resmi 1 tahun.</p>', 'Jam81.jpg', 1),
('JM009', 'Alexandre Christie Jam Tangan Pria - Cok', 1550000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Analog watch</li>\r\n   <li>Material : Stainless steel case & genuine leather strap</li>\r\n   <li>Water resistant 50 m</li>\r\n   <li>Japan Quartz movement</li>\r\n   <li>Tampilan 3 chrono eyes pada center dial</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Cokelat - Strap Leather - 6374MCLBRBA</b></p>\r\n\r\n\r\n<p>Alexandre Christie 6374MCLBRBA Rose Gold Jam Tangan Pria, analog watch yang memadukan rose gold stainless steel case, black dial dan brown stiched genuine leather strap. Jam tangan dengan tampilan 3 chrono eyes pada center dial, beige accent index & arabic numeral dan date display window pada posisi angka 4-5 ini menggunakan Japan Quartz movement serta memiliki water resistant 50 m.</p>', 'Jam91.jpg', 1),
('JM0010', 'Alexandre Christie Jam Tangan Pria Silve', 2080000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Petunjuk Jam / Tanggal.</li>\r\n   <li>Fitur Chronograph Stopwatch.</li>\r\n   <li>Battery Quartz Jepang.</li>\r\n   <li>Water Resist 50 Meter.</li>\r\n   <li>Rantai Jam All Stainless Steel.</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie- Jam Tangan Pria - Silver Black Red - Stainless Steel- AC 6345 MSBR</b></p>\r\n\r\n\r\n<p>Jam tangan Alexandre Christie untuk Pria, dilengkapi dengan petunjuk Jam / Tanggal , Chronograph Stopwatch, Water Resist hingga kedalaman 50meter, Diameter jam : 4.4cm, Tebal Jam : 0.8cm, Lebar Tali : 2.1cm. Bergaransi resmi 1 tahun.</p>', 'Jam101.jpg', 1),
('JM0011', 'Alexandre Christie Jam Tangan Pria - Str', 1701860, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Analogue Chronograph</li>\r\n   <li>Fitur : Chronograph Aktif dan Tanggal</li>\r\n   <li>Tali : Rantai Stainless</li>\r\n   <li>Ketahanan Air : Hingga tekanan 50 Meter.</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Strap Stainless Steel - Silver Hitam - AC6291LH</b></p>\r\n\r\n\r\n<p>Jam tangan dari brand ternama sudah pasti membuat anda semakin percaya diri menggunakanya, dengan desain yang exclusive dan fitur yang melimpah. Bukan hanya itu, Jam tangan ini didukung dengan material yang tangguh dan berkelas dengan all stainless steel murni berkualitas tinggi.Chronograph dan fitur tanggal yang berguna bagi kegiatan harian anda. Ketangguhan bukan hanya pada material dan design tapinjuga pada mesin yang mampu bertahan dalam tekanan air hingga 50 meter.</p>', 'Jam111.jpg', 1),
('JM0012', 'Alexandre Christie Jam Tangan Pria Kulit', 1999000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>tahan air 50 M</li>\r\n   <li>trendi</li>\r\n   <li>berkualitas</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria Kulit - 6308MCBBRBO - Hitam</b></p>\r\n\r\n\r\n<p>Alexandre Christie 6308MCBBRBO. Jam tangan Pria yang memadukan rosegold stainless steel case & black letaher strap dan black dial. Jam tangan yang di desain casual dengan tampilan 3 chrono eyes pada center dial, black bezel, rosegold index numeral dan penunjuk tanggal & hari di arah jam 3 ini menggunakan mesin Japan Quartz Movement serta tahan air hingga kedalaman 50m.Dengan perpaduan warna hitam dan rosegold, jam tangan ini cocok di pakai sehari-hari maupun untuk menghadiri suatu acara. Bergaransi resmi 1 tahun.</p>', 'Jam12.jpg', 1),
('JM0013', 'Alexandre Christie Jam Tangan Pria - Str', 1465646, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Analogue Chronograph</li>\r\n   <li>Fitur : Chronograph Aktif dan Tanggal</li>\r\n   <li>Tali : Rantai Stainless</li>\r\n   <li>Ketahanan Air : Hingga tekanan 50 Meter.</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Jam Tangan Pria - Strap Stainless Steel - Silver Putih - AC6291LH</b></p>\r\n\r\n\r\n<p>Jam tangan dari brand ternama sudah pasti membuat anda semakin percaya diri menggunakanya, dengan desain yang exclusive dan fitur yang melimpah. Bukan hanya itu, Jam tangan ini didukung dengan material yang tangguh dan berkelas dengan all stainless steel murni berkualitas tinggi.Chronograph dan fitur tanggal yang berguna bagi kegiatan harian anda. Ketangguhan bukan hanya pada material dan design tapinjuga pada mesin yang mampu bertahan dalam tekanan air hingga 50 meter.</p>', 'Jam13.jpg', 1),
('JM0014', 'Alexandre Christie Man 6350 Putih Stainl', 2500000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Petunjuk Jam / Tanggal</li>\r\n   <li>Chronograph Stopwatch</li>\r\n   <li>Battery Quartz</li>\r\n   <li>Water Resist 50 Meter</li>\r\n   <li>Garansi Resmi 1 Tahun</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie Man 6350 - Putih - Jam Tangan Stainless</b></p>\r\n\r\n\r\n<p>Jam tangan Alexandre Christie untuk Pria, dilengkapi dengan petunjuk jam / tanggal / Chronograph Stopwatch, Water Resist hingga kedalaman 50 meter, Diameter jam : 4.6cm, Tebal Jam : 1.1cm, Lebar Tali Rantai : 2.2cm. Bergaransi resmi 1 tahun.</p>', 'Jam14.jpg', 1),
('JM0015', 'Alexandre Christie - Sportage Jam Tangan', 2200000, 'Unit', '<p><b>Spesifikasi</b></p><ul>\r\n\r\n   <li>Petunjuk Jam / Tanggal / Stopwatch</li>\r\n   <li>Battery Quartz Jepang</li>\r\n   <li>Water Resist 50 Meter</li>\r\n   <li>Kaca Jam dari Mineral Kristal</li>\r\n   </ul>\r\n\r\n<p><b>Detail produk dari Alexandre Christie - Sportage Jam Tangan Pria - Silver White - Stainless Steel - AC 6383 MSW</b></p>\r\n\r\n\r\n<p>Alexandre Christie adalah brand jam tangan paling populer di Indonesia saat ini. Didukung oleh komunitas online yang agresif dari segi marketing maupun offline showcase, tidak heran Alexandre Christie memiliki tempat yang nyaman di hati masyarakat kita. Berbagai varian segmen jam tangan diciptakan oleh Alexandre Christie. Siapapun anda, apapun profesi/rutinitas anda, temukan pendamping waktu anda di Alexandre Christie.\r\n\r\nJam tangan Alexandre Christie untuk Pria, dilengkapi dengan petunjuk Jam / Tanggal / Stopwatch, Water Resist hingga kedalaman 50meter,  (Diameter jam : 4.6cm, Tebal Jam : 1.4cm, Lebar Tali : 2.4cm). Bergaransi resmi 1 tahun.</p>', 'Jam15.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `cart_nomor` int(11) NOT NULL,
  `cart_customer_id` int(11) NOT NULL,
  `cart_tanggal` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_nomor`, `cart_customer_id`, `cart_tanggal`) VALUES
(11, 1, '2016-01-13 15:57:41');

-- --------------------------------------------------------

--
-- Table structure for table `cart_detail`
--

CREATE TABLE IF NOT EXISTS `cart_detail` (
  `cart_nomor` int(11) NOT NULL,
  `cart_barang_id` varchar(10) NOT NULL,
  `cart_barang_qty` int(11) NOT NULL,
  `cart_barang_harga` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_detail`
--

INSERT INTO `cart_detail` (`cart_nomor`, `cart_barang_id`, `cart_barang_qty`, `cart_barang_harga`) VALUES
(11, 'Jam07', 1, 220000),
(11, 'Jam02', 1, 136000),
(11, 'Jam03', 1, 76500),
(11, 'Jam05', 1, 135000),
(11, 'Jam01', 1, 102000);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('214968c27d8cd1495bd2c2af2252d2e7', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36', 1452774293, 'a:1:{s:9:"user_data";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`customer_id` int(11) NOT NULL,
  `customer_nama` varchar(40) NOT NULL,
  `customer_alamat` text NOT NULL,
  `customer_telepon` varchar(15) NOT NULL,
  `customer_email` varchar(40) NOT NULL,
  `customer_password` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_nama`, `customer_alamat`, `customer_telepon`, `customer_email`, `customer_password`) VALUES
(1, 'Tikko', 'Pekalongan', '085742435903', 'tikko@yahoo.com', 'ga0W4rBDttEnTHddNazuQMfRqGLT7xiqHAKE/kBnPBYrURUEyvWz3/fywih2n3pqttU4l/ajbcVbR+gzYeftxQ==');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`admin_id`), ADD UNIQUE KEY `customer_email` (`admin_email`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
 ADD PRIMARY KEY (`barang_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
 ADD PRIMARY KEY (`cart_nomor`);

--
-- Indexes for table `cart_detail`
--
ALTER TABLE `cart_detail`
 ADD PRIMARY KEY (`cart_nomor`,`cart_barang_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`customer_id`), ADD UNIQUE KEY `customer_email` (`customer_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
